
import java.util.Scanner;

public class Arvore {

	// vamos colocar 10 n�meros em uma �rvore.
	// consultar os numeros pares.

	// declarar a minha �rvore.
	private static class ARVORE {
		public int numero;
		public ARVORE direita, esquerda;
	}

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		// vamos instanciar uma �rvore vazia.
		ARVORE raiz = null;
		int opcao, achou;

		do {
			System.out.println("Galera construindo uma �rvore.");
			System.out.println("1=Preencher a �rvore.");
			System.out.println("2=Imprimir os pares.");
			System.out.println("3=Imprimir os impares.");// desafio
			System.out.println("4=Imprimir a arvore em ordem.");
			System.out.println("5=Imprimir numeros primos.");// desafio pos ordem/ pre ordem
			System.out.println("6=Sair.");

			System.out.println("Escolha uma op��o:");
			opcao = sc.nextInt();

			// vamos tratar os poss�eis erros.
			if (opcao < 1 || opcao > 6) {
				System.out.println("Op��o inv�lida!");
			} else if (opcao == 1) {
				// vamos preencher a �rvore
				raiz = null;
				for (int i = 1; i <= 10; i++) {
					System.out.print("Informe o nmero a ser inserido: ");
					int numero = sc.nextInt();

					raiz = inserir(raiz, i - 1);

				}

			} else if (opcao == 2) {

				// vamos imprimir os pares
				if (raiz == null) {
					System.out.println("Sua rvore est vazia, utilize a opo 1 para preencher");
				} else {
					achou = 0;
					achou = consultarPares(raiz, achou);
					if (achou == 0) {
						System.out.println("n�o existem n�meros pares!");
					}
				}

			} else if (opcao == 3) {
				// vamos imprimir os impares.
				if (raiz == null) {
					System.out.println("Sua �rvore est� vazia, utilize a op��o 1 para preencher.");

				} else {
					achou = 0;
					achou = consultarImpares(raiz, achou);
					if (achou == 0) {
						System.out.println("n�o h� impares.");
					}
				}

			}
			if (opcao == 4) {
				if (raiz == null) {
					System.out.println("Sua �rvore est� vazia, utilize a op��o 1 para preencher.");
				} else {
					achou = 0;
					achou = consultarPrimos(raiz, achou);
					if (achou == 0) {
						System.out.println("n�o existem n�meros primos.");
					}

				}
			} else if (opcao == 5) {
				if (raiz == null) {
					System.out.println("Sua �rvore est� vazia, utilize a op��o 1 para preencher.");

				} else {
					System.out.println("Numeros em ordem:");
					consultarOrdem(raiz);

				}

			} else if (opcao == 6) {
				if (raiz == null) {
					System.out.println("Sua rvore est vazia, utilize a opo 1 para preencher");
				} else {
					System.out.println("Os nmeros da rvore em pr-ordem so: ");
					// chamar mtodo de consultar
					consultarPreOrdem(raiz);
				}

			}

		} while (opcao != 8); // essa ser� a nossa op��o de finalizar o programa.

	}

	public static ARVORE inserir(ARVORE aux, int nummero) {
		// primeiro passo: sera que a arore ainda n�o tem dado?
		if (aux == null) {
			aux = new ARVORE();
			aux.numero = nummero;
			aux.direita = null;
			aux.esquerda = null;
		} else if (nummero < aux.numero) {
			aux.esquerda = inserir(aux.esquerda, nummero);
		} else {
			aux.direita = inserir(aux.direita, nummero);
		}
		return aux;
	}

	public static int consultarPares(ARVORE aux, int achou) {
		if (aux != null) {
			if (aux.numero % 2 == 0) {
				System.out.println("Os n�meros pares s�o:" + aux.numero + " ");
				achou = 1;

			}
			achou = consultarPares(aux.esquerda, achou);
			achou = consultarPares(aux.direita, achou);
		}
		return achou;
	}

	public static int consultarImpares(ARVORE aux, int achou) {
		if (aux != null) {
			if (aux.numero % 2 != 0) {
				System.out.println("Os n�meros impares s�o:" + aux.numero + " ");
				achou = 1;

			}
			achou = consultarImpares(aux.esquerda, achou);
			achou = consultarImpares(aux.direita, achou);
		}
		return achou;
	}

	public static void consultarOrdem(ARVORE aux) {
		if (aux != null) {
			consultarOrdem(aux.esquerda);
			System.out.println("N�mero: " + aux.numero + " ");
			consultarOrdem(aux.direita);

		}
	}

	public static int consultarPrimos(ARVORE aux, int achou) {
		if (aux != null) {
			int cont = 0;
			for (int i = 2; i <= aux.numero; i++) {
				if (aux.numero % i == 0) {
					cont++;
				}
			}
			if (cont == 1) {
				System.out.println("Nmero primo: " + aux.numero);
				achou = 1;
			}
			achou = consultarPrimos(aux.esquerda, achou);
			achou = consultarPrimos(aux.direita, achou);
		}
		return achou;
	}

	public static void consultarPreOrdem(ARVORE aux) {
		if (aux != null) {
			if (aux.esquerda != null) {
				System.out.println("Esquerda: " + aux.numero);
				consultarPreOrdem(aux.esquerda);
			}
			if (aux.direita != null) {
				System.out.println("Direita: " + aux.numero);
				consultarPreOrdem(aux.direita);
			}
		}
	}

}
